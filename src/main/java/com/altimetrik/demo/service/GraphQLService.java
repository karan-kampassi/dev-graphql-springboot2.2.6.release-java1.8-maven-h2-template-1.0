package com.altimetrik.demo.service;

import com.altimetrik.demo.service.datafetcher.AllUsersFetchers;
import com.altimetrik.demo.service.datafetcher.UserFetcher;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import io.leangen.graphql.GraphQLSchemaGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.stream.Stream;

@Service
public class GraphQLService {

    @Autowired
    UserService userService;

    private GraphQL graphQL;

    @Autowired
    private AllUsersFetchers allUsersFetchers;

    @Autowired
    private UserFetcher userFetcher;

    // load schema at application start up
    @PostConstruct
    private void loadSchema() throws IOException {
        GraphQLSchema schema = new GraphQLSchemaGenerator().withOperationsFromSingleton(userService).generate();
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    public GraphQL getGraphQL() {
        return graphQL;
    }
}
